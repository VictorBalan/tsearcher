/** 
*@module BaseController
*@author Victor Balan
*/
var marklogic = require('marklogic');
var mlutils = require('../util/mlutils');
var db = marklogic.createDatabaseClient(mlutils.connInfo);
var qb = marklogic.queryBuilder;


exports.testQbe = function(callback){
	var query = qb.where(
			qb.collection('link'),
			qb.scope(qb.property('keyPhrases'), qb.word('key', 'java'))
		).calculate(
			qb.facet("count", "postId")
		).withOptions({categories: 'none'}).withOptions({metrics: true, categories: 'none'})

	db.documents.query(query).result(function(documents){
		var postIds = [];
		var data = documents[0].facets.count.facetValues;

		for(var i=0;i<data.length;i++){
			postIds.push(data[i].value);
		}
		var sort = qb.sort(qb.property('username'), "ascending");
		var query = qb.where(qb.collection("post"),qb.or(qb.word("content", "java"), qb.value("postId", postIds))).calculate(
			qb.facet("count", "username")
			).withOptions({categories: 'none'}).orderBy(sort);

		db.documents.query(query).result(function(documents){
			callback(null, {data: documents[0].facets.count.facetValues})
		}, function(error){
			callback(500, error);
		});
	}, function(error){
		callback(500, error);
	});
}
exports.testQbe2 = function(callback){
  	var sort = qb.sort(qb.property('username'), "ascending");
	var query = qb.where(qb.collection("post"),getWordQuery("java")).calculate(
		qb.facet("count", "username")//qb.geoPropertyPair("geolocation", "lat", "lng"))
		).withOptions({categories: 'none'}).orderBy(sort);

	db.documents.query(query).result(function(documents){
		callback(null, {data: documents[0].facets.count.facetValues})
	}, function(error){
		callback(500, error);
	});
}
exports.testQbeGeo = function(callback){
	var query = qb.where(qb.collection("post")).calculate(
		qb.facet("count", "geolocation")//qb.geoPropertyPair("geolocation", "lat", "lng"))
		);

	db.documents.query(query).result(function(documents){
		callback(null, {data: documents})
	}, function(error){
		callback(500, error);
	});
}

exports.peopleSearchTotal = function(toFind, callback){	
	var sort = qb.sort(qb.property('username'), "ascending");
	var query = qb.where(qb.collection("post"),qb.word("content", toFind)).calculate(
		qb.facet("count", "username")
		).withOptions({categories: 'none'}).orderBy(sort);

  getQueryTotalEntries(query, callback);
}

exports.peopleSearchTotal2 = function(toFind, callback){	
	var query = qb.where(
		qb.collection('link'),
		qb.scope(qb.property('keyPhrases'), 
			qb.word('key', toFind))
	).calculate(
		qb.facet("count", "postId")
	).withOptions({categories: 'none'}).withOptions({metrics: true, categories: 'none'});

	db.documents.query(query).result(function(documents){
		var postIds = [];
		var data = documents[0].facets.count.facetValues;

		for(var i=0;i<data.length;i++){
			postIds.push(data[i].value);
		}
		var sort = qb.sort(qb.property('username'), "ascending");
		var query = qb.where(qb.collection("post"),qb.or(qb.word("content", toFind), qb.value("postId", postIds))).calculate(
			qb.facet("count", "username")
			).withOptions({categories: 'none'}).orderBy(sort);

			getQueryTotalEntries(query, callback);
	}, function(error){
		callback(500, error);
	});

}

exports.peopleSearch2 = function(toFind, pageNo, resultsPerPage, callback){
	var query = qb.where(
		qb.collection('link'),
		qb.scope(qb.property('keyPhrases'),
		qb.word('key', toFind))
	).calculate(
		qb.facet("count", "postId")
	).withOptions({categories: 'none'}).withOptions({metrics: true, categories: 'none'})

	db.documents.query(query).result(function(documents){
		var postIds = [];
		var data = documents[0].facets.count.facetValues;

		for(var i=0;i<data.length;i++){
			postIds.push(data[i].value);
		}
		var sort = qb.sort(qb.property('username'), "ascending");
		var query = qb.where(qb.collection("post"),qb.or(qb.word("content", toFind), qb.value("postId", postIds))).calculate(
			qb.facet("count", "username")
			).withOptions({categories: 'none'}).orderBy(sort);

		db.documents.query(query).result(function(documents){
			callback(null, {data: documents[0].facets.count.facetValues})
		}, function(error){
			callback(500, error);
		});
	}, function(error){
		callback(500, error);
	});
}

exports.peopleSearch = function(toFind, pageNo, resultsPerPage, callback){
	console.log("Page: " + pageNo + ". Results per page: " + resultsPerPage);
  
	var sort = qb.sort(qb.property('username'), "ascending");
	var query = qb.where(qb.collection("post"),qb.word("content", toFind)).calculate(
		qb.facet("count", "username")
		).withOptions({categories: 'none'}).orderBy(sort);

	db.documents.query(query).result(function(documents){
		callback(null, {data: documents[0].facets.count.facetValues})
	}, function(error){
		callback(500, error);
	});
}

exports.basicSearchTotal = function(toFind, callback){
  var sort = qb.sort(qb.property('username'), "ascending");
  var query = qb.where(qb.collection("post"),getWordQuery(toFind)).orderBy(sort);

  getQueryTotalEntries(query, callback);
}

exports.basicSearch = function(toFind, pageNo, resultsPerPage, callback){
  console.log("Page: " + pageNo + ". Results per page: " + resultsPerPage);
  
  var sort = qb.sort(qb.property('username'), "ascending");
  var query = qb.where(qb.collection("post"),getWordQuery(toFind)).orderBy(sort);

  var options = {
  	isPagedQuery: true,
  	pageNo: pageNo,
  	resultsPerPage: resultsPerPage,
  	withLocationMap: true,
  	withDateMap: true
  }

  doQuery(query, options, callback);
}

function getWordQuery(string){
	string = string.split(" ");

    var toReturn = [];
	for(var i=0;i<string.length; i++){
		toReturn.push(string[i]);
	}
	return qb.word("content", string);
}

function getQueryByExampleForSplitString(string){
	string = string.split(" ");

    var toReturn = [];
	for(var i=0;i<string.length; i++){
		toReturn.push({content: {$word: string[i]}});
	}
	return qb.byExample(toReturn);
}

exports.countQuery = function(pageNo, resultsPerPage, callback){
  console.log("Page: " + pageNo + ". Results per page: " + resultsPerPage);
  
  var sort = qb.sort(qb.property('username'), "ascending");
  var query = qb.where().orderBy(sort);

  var options = {
  	isPagedQuery: true,
  	pageNo: pageNo,
  	resultsPerPage: resultsPerPage,
  	withLocationMap: true,
  	withDateMap: true
  }

  doQuery(query, options, callback);
}

exports.countQueryTotal = function(callback){
  var sort = qb.sort(qb.property('username'), "ascending");
  var query = qb.where(qb.collection("link")).orderBy(sort);

  getQueryTotalEntries(query, callback);
}

function getQueryTotalEntries(query, callback){
  db.documents.query(query.slice(0)).result(function(documents) {
      callback(null, {total: documents[0].total});
  }, function(error) {
      callback(500, error);
  }); 
}

function doQuery(query, options, callback) {
	if(options.isPagedQuery){
		var resultsPerPage = parseInt(options.resultsPerPage);
		var offset = parseInt(options.pageNo) * resultsPerPage;
		query = query.slice(offset, resultsPerPage);
	}

	db.documents.query(query).result(function(documents){
		var result = {};
		if(options.withLocationMap){
			result.locationMap = constructLocationMap(documents);
		}
		if(options.withDateMap){
			var dateHourMaps = constructDateMap(documents);;
			result.dateMap = dateHourMaps.dateMap;
			result.hourMap = dateHourMaps.hourMap
		}

		if(options.withPeople){

		}

		callback(null, result);
	}, function(error){
		callback(500, error);
	})
}

function constructDateMap(documents){
	var dateMap = {};
	var hourMap = {};
	for(var i=0;i<documents.length;i++){
		var createdAt = documents[i].content.createdAt.split(" ")[0];
		var hourAt = documents[i].content.createdAt.split(" ")[1];
		if(dateMap[createdAt]==undefined){
			dateMap[createdAt] = {
				date: createdAt,
				count: 0
			}
		}
		dateMap[createdAt].count++;
		if(hourMap[hourAt]==undefined){
			hourMap[hourAt] = {
				hour: hourAt,
				count: 0
			}
		}
		hourMap[hourAt].count++;
	}

	return {dateMap: dateMap, hourMap: hourMap};
}

function constructLocationMap(documents){
	var locationMap = {};
	for(var i=0;i<documents.length;i++){
		var location = documents[i].content.processedLocation;
		var geolocation = documents[i].content.geolocation;
		//TODO - refactor to add all geolocations with their count
		if(locationMap[location]==undefined){
		// 	if(geolocation!=undefined
		// 		&& !containsGeoLocation(locationMap[location].geolocationList, geolocation)){
		// 		locationMap[location].geolocationList.push(geolocation);
		// 	}
		// }else{
			locationMap[location] = {
				name: location,
				count: 0,
				geolocation: geolocation
			}
		}
		locationMap[location].count++;
	}

	return locationMap;
}

function containsGeoLocation(geolocationList, geolocation){
	for(var i=0;i<geolocationList.length;i++){
		if(geolocationList[i].lat==geolocation.lat 
			&& geolocationList[i].lng==geolocation.lng){
			return true;
		}
	}
	return false;
}