var BaseController = require("./controller/baseController")

module.exports = function(app){
	var responseFunction = function(res, err, response){
		if(err){
			res.send(response, err)
		}else{
			res.json(response)
		}
	}

	app.get("/test", function(req, res){
		BaseController.testQbe(function(err, response){
			responseFunction(res, err, response);
		})
	})

	app.post("/count/", function(req, res){
		var pageNo = req.body.pageNo;
		var resultsPerPage = req.body.resultsPerPage;
		BaseController.countQuery(pageNo, resultsPerPage, function(err, response){
			responseFunction(res, err, response);
		})
	})

	app.get("/count/total", function(req, res){
		BaseController.countQueryTotal(function(err, response){
			responseFunction(res, err, response);
		})
	})

	app.get("/search/basic/total/:toFind", function(req, res){
		BaseController.basicSearchTotal(req.params.toFind, function(err, response){
			responseFunction(res, err, response);
		})
	})

	app.post("/search/basic/", function(req, res){
		var pageNo = req.body.pageNo;
		var resultsPerPage = req.body.resultsPerPage;
		var toFind = req.body.toFind;
		BaseController.basicSearch(toFind ,pageNo, resultsPerPage, function(err, response){
			responseFunction(res, err, response);
		})
	})

	app.get("/search/people/total/:toFind", function(req, res){
		BaseController.peopleSearchTotal2(req.params.toFind, function(err, response){
			responseFunction(res, err, response);
		})
	})

	app.post("/search/people", function(req, res){
		var pageNo = req.body.pageNo;
		var resultsPerPage = req.body.resultsPerPage;
		var toFind = req.body.toFind;
		BaseController.peopleSearch2(toFind, pageNo, resultsPerPage, function(err, response){
			responseFunction(res, err, response);
		})
	})

}