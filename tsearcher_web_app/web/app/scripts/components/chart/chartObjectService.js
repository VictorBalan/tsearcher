'use strict';
angular.module('tsearcher.services').service('ChartObjectService', function() {
  var chartTypes = {pie: "PieChart", bar: "BarChart", col: "ColumnChart"}

  this.newCountPieChartObject = function(title){
    var object = {};
    object.data =  {"cols": [{id: "l", label: "Label", type: "string"},
                            {id: "c", label: "Count", type: "number"}],
                    "rows": []};

    // $routeParams.chartType == BarChart or PieChart or ColumnChart...
    object.type = chartTypes.pie;
    object.options = {'title': title};
    return object;
  }

  this.newCountPieChartRow = function(label, value){
    return {c: [{v: label}, {v: value}]};
  }
});