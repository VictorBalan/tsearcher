'use strict'

angular.module('tsearcher.routes').config(['$routeProvider', function($routeProvider){
	$routeProvider
		.when('/people', {
			templateUrl: 'scripts/components/people/people.html',
			controller: 'PeopleController',
			controllerAs: 'vm'
    	});
}]);