'use strict';

angular.module('tsearcher.controllers')
.controller('PeopleController', PeopleController);

function PeopleController($scope, PostService){
	var vm = this;

	vm.shit = "data";
	vm.people = [];

	vm.peopleSearch = function(){
		vm.people = []
		PostService.peopleSearch(vm.searchText, function(data){
			if(data.percentLoaded == 100){
				var people = data.data
				people.sort(function(a, b) {
				    return b.count - a.count;
				});
				console.log(people);
				vm.people = people;
			}
		})
	}
}