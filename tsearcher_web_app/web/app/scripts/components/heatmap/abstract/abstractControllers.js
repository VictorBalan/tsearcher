angular.module('tsearcher.controllers')
.controller('AbstractMapController', AbstractMapController)
.controller('AbstractChartController', AbstractChartController)
.controller('AbstractLocationDataController', AbstractLocationDataController)
.controller('AbstractTimelineController', AbstractTimelineController);

//MUST IMPLEMENT $scope.loadCountData AFTER EXTENDING
//TODO - implement a directive doing this job
function AbstractMapController($scope, HeatLayerService, HeatmapService, uiGmapGoogleMapApi){
    var  vm = this;

    uiGmapGoogleMapApi.then(function(maps) {
    $scope.map = {
      center: {
        latitude: 45.834540,
        longitude: 24.818115 
      },
      zoom: 6,
      showHeat: true,
      heatLayerCallback: function(layer) {
        var heatLayer = HeatLayerService.newCustomHeatLayer(layer);
        heatLayer.changeRadius(20);
        heatLayer.changeGradient();
        vm.heatLayer = heatLayer;
      },
      options:{
        mapTypeId: maps.MapTypeId.HYBRID
      },
      control: {},
      refresh: {}
    };

    $scope.loadHeatPoints = function(){
        $scope.resetGoogleMap()
        vm.heatLayer.setData($scope.getHeatPoints($scope.locationData));
    }

    $scope.resetGoogleMap = function(){
        vm.heatLayer.setMap(null);
        HeatmapService.resetData();
        vm.heatLayer = HeatLayerService.newCustomHeatLayer(new maps.visualization.HeatmapLayer());
        vm.heatLayer.setMap($scope.map.control.getGMap());
        vm.heatLayer.changeRadius(20);
        vm.heatLayer.changeGradient();
    }

    $scope.getHeatPoints = function(data){
        var maxCount = 0;

        for(var key in data) { 
          if(data.hasOwnProperty(key) && data[key].count>maxCount) { 
            maxCount = data[key].count;
          } 
        }

        for(var key in data) { 
          if(data.hasOwnProperty(key)) { 
            if(data[key].geolocation!=undefined){
              HeatmapService.addPoint(maps, 
                data[key].geolocation.lat, 
                data[key].geolocation.lng,
                data[key].count, maxCount);
            }
          } 
        }
        return new maps.MVCArray(HeatmapService.getPoints()); 
    }
  });
}

function AbstractChartController($scope, ChartObjectService){

  $scope.constructChartObject = function(){
    $scope.locationsChartRows.length = 0;
    for(var key in $scope.locationData) { 
      if($scope.locationData.hasOwnProperty(key)) { 
        if($scope.locationData[key].geolocation!=undefined){
          $scope.locationsChartRows.push(ChartObjectService
            .newCountPieChartRow($scope.locationData[key].name, $scope.locationData[key].count));
        }
      } 
    }
  }

  function initializeChartObject(){
    $scope.locationsChartObject = ChartObjectService.newCountPieChartObject('Tweet count for location')
    $scope.locationsChartRows = [];
    $scope.locationsChartObject.data.rows = $scope.locationsChartRows;
  }

  initializeChartObject();
}

function AbstractLocationDataController($scope){
  $scope.locationData = [];

  $scope.computeLocationData = function(data){
    for(var key in data) { 
      if(data.hasOwnProperty(key)) { 
        if($scope.locationData[key]==undefined){
          $scope.locationData[key] = {};
          $scope.locationData[key].name = data[key].name;
          $scope.locationData[key].count = data[key].count;
          $scope.locationData[key].geolocation = data[key].geolocation;
        }else{
          $scope.locationData[key].count = $scope.locationData[key].count + data[key].count;
        }
      } 
    }
  }

  $scope.resetLocationData = function(){
    $scope.locationData = [];
  }
}

function AbstractTimelineController($scope){
  $scope.dateData = [];

  $scope.timedChartObject = {};
  $scope.timedChartObject.options ={
    displayAnnotations: true
  }
  $scope.timedChartObject.type = "AnnotationChart"

  $scope.timedChartRows = [];

  $scope.timedChartObject.data = {
    cols:[
      {id: "month", label:"Month", type: "datetime"},
      {id: "count", label:"Count", type: "number"}
    ],
    rows: $scope.timedChartRows
  }

  $scope.loadTimeChartData = function(){
    $scope.timedChartRows.length = 0;
    for(var key in $scope.dateData){
      if($scope.dateData.hasOwnProperty(key)){
        $scope.timedChartRows.push(constructRow(key, $scope.dateData[key].count)); 
      }
    }
  }


  $scope.computeDateData = function(data){
    for(var key in data) { 
      if(data.hasOwnProperty(key)) { 
        // var dateDataKey = key.split("-")[0];
        // var dateDataKey = key.split("-")[0] + "-" + key.split("-")[1];
        var dateDataKey = key;
        if($scope.dateData[dateDataKey]==undefined){
          $scope.dateData[dateDataKey] = {};
          $scope.dateData[dateDataKey].date = dateDataKey;
          $scope.dateData[dateDataKey].count = data[key].count;
        }else{
          $scope.dateData[dateDataKey].count = $scope.dateData[dateDataKey].count + data[key].count;
        }
      } 
    }
  }

  function constructRow(date, value){
    return {c: [{v: new Date(date)},{v: value}]};
  }
}