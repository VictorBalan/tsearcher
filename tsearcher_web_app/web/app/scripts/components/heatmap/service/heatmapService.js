'use strict';
angular.module('tsearcher.services').service('HeatmapService', function() {
  var dataPoints = [];

  this.addPoint = function(maps, lat, lon, count, maxCount) {
    var weight = this.weight(count, maxCount);
    var newPoint = {
      location: new maps.LatLng(lat, lon),
      weight: weight
    };
    dataPoints.push(newPoint);
    return newPoint;
  };

  this.getPoints = function() {
    return dataPoints;
  };

  this.resetData = function(){
    dataPoints = [];
  }

  this.weight = function(count, maxCount) {
    var ratio = count/maxCount;
    // if(ratio<0.1){
    //   return 0.1
    // }
    return count/maxCount;
  };
});