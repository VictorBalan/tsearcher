'use strict';
angular.module('tsearcher.services').service('PostService', function($http) {
  var service = {};

  service.count = count;
  service.basicSearch = basicSearch;
  service.peopleSearch = peopleSearch;

  return service;
  //********************** BASE STUFF *********************
  function doQuery(totalQuery, startMainQuery){
    totalQuery.then(function(response){
      console.log("Total entries: " + response.data.total);

      var resultsPerPage = 10000;
      var totalPages = Math.ceil(response.data.total/resultsPerPage);

      startMainQuery({pageNo: 0, resultsPerPage: resultsPerPage, totalPages: totalPages});
    }, function(err){
      console.log(err);
    })
  }

  function processMainQueryResponse(response, options, callback, doNext){
      response.data.percentLoaded = Math.round((options.pageNo/options.totalPages) * 100);
      callback(response.data);
      if(options.pageNo != options.totalPages){
        options.pageNo++;
        doNext(options);
      }
  }

  //********************** COUNT STUFF *********************
  function count(callback){
    doQuery(countTotal(), function(options){
      countQuery(options, callback);
    })
  }

  function countTotal(){
    return $http.get("http://localhost:8088/count/total");
  }

  function countQuery(options, callback){
    $http.post("http://localhost:8088/count/", options).then(function(response){
      processMainQueryResponse(response, options, callback, function(options){
        countQuery(options, callback);
      })
    }, function(err){
      console.log(err);
    });
  }
  //******************** END COUNT STUFF *******************
  //********************* SEARCH STUFF *********************
  function basicSearch(toFind, callback){
    doQuery(basicSearchTotal(toFind), function(options){
      options.toFind = toFind;
      basicSearchQuery(options, callback);
    })
  }

  function basicSearchTotal(toFind){
    return $http.get("http://localhost:8088/search/basic/total/" + toFind);
  }

  function basicSearchQuery(options, callback){
    $http.post("http://localhost:8088/search/basic/", options).then(function(response){      
      processMainQueryResponse(response, options, callback, function(options){
        basicSearchQuery(options, callback);
      })
    }, function(err){
      console.log(err);
    });
  }
  //******************* END SEARCH STUFF *******************
  //***************** PEOPLE SEARCH STUFF ******************
  function peopleSearch(toFind, callback){
    doQuery(peopleSearchTotal(toFind), function(options){
      options.toFind = toFind;
      peopleSearchQuery(options, callback);
    })
  }
  function peopleSearchTotal(toFind){
    return $http.get("http://localhost:8088/search/people/total/" + toFind);
  }

  function peopleSearchQuery(options, callback){
    $http.post("http://localhost:8088/search/people/", options).then(function(response){   
      processMainQueryResponse(response, options, callback, function(options){
        peopleSearchQuery(options, callback);
      })
    }, function(err){
      console.log(err);
    });
  }
  //*************** END PEOPLE SEARCH STUFF ****************
});