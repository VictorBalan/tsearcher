'use strict';
angular.module('tsearcher.services').service('HeatLayerService', function() {
  var service = {};

  service.newCustomHeatLayer = createCustomHeatLayer;
  return service;


  function createCustomHeatLayer(heatLayer) {
    this.setData = function(dataPointList){
      heatLayer.setData(dataPointList);
    }

    this.getData = function(){
      return heatLayer.getData();
    }

    this.changeRadius = function(value) {
       heatLayer.set('radius', value);
    }

    this.changeOpacity = function(value) {
       heatLayer.set('opacity', value);
    }

    this.changeGradient = function() {
       var gradient = [
           'rgba(0, 255, 255, 0)',
           'rgba(0, 255, 255, 1)',
           'rgba(0, 191, 255, 1)',
           'rgba(0, 127, 255, 1)',
           'rgba(0, 63, 255, 1)',
           'rgba(0, 0, 255, 1)',
           'rgba(0, 0, 223, 1)',
           'rgba(0, 0, 191, 1)',
           'rgba(0, 0, 159, 1)',
           'rgba(0, 0, 127, 1)',
           'rgba(63, 0, 91, 1)',
           'rgba(127, 0, 63, 1)',
           'rgba(191, 0, 31, 1)',
           'rgba(255, 0, 0, 1)'
       ]
       heatLayer.set('gradient', heatLayer.get('gradient') ? null : gradient);
    }

    this.setMap = function(map){
      heatLayer.setMap(map);
    }
    return this;
  };
});