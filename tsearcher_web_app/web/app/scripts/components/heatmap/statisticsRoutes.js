'use strict'

angular.module('tsearcher.routes').config(['$routeProvider', function($routeProvider){
	$routeProvider
		.when('/count', {
			templateUrl: 'scripts/components/heatmap/count/google-map.html',
			controller: 'CountController',
			controllerAs: 'vm'
    	})
		.when('/search', {
			templateUrl: 'scripts/components/heatmap/search/search-page.html',
			controller: 'SearchController'
    	});
}]);