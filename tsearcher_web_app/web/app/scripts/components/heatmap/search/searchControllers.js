'use strict';

angular.module('tsearcher.controllers')
.controller('SearchController', SearchController);

function SearchController($scope, $controller, PostService){
	$scope.dataLoadedPercent = 100;
	$scope.searchText = {
		basicSearch: ''
	};

	$scope.basicSearch = function(text){
      	$scope.dataLoadedPercent = 0;
      	$scope.resetLocationData();
		console.log($scope.locationData);
	    PostService.basicSearch(text, function(data){
	      $scope.computeLocationData(data.locationMap);

	      $scope.loadHeatPoints();
	      $scope.constructChartObject();

	      $scope.computeDateData(data.dateMap);
	      $scope.loadTimeChartData();
	      
	      // $scope.computeDateData(data.dateMap);
	      // $scope.computeHourData(data.hourMap);
	      $scope.dataLoadedPercent = data.percentLoaded;
	    })
	}

	angular.extend(this, $controller('AbstractLocationDataController', {$scope: $scope}));
  	angular.extend(this, $controller('AbstractChartController', {$scope: $scope}));
  	angular.extend(this, $controller('AbstractTimelineController', {$scope: $scope}));
	angular.extend(this, $controller('AbstractMapController', {$scope: $scope}));
}