'use strict';

angular.module('tsearcher.controllers')
.controller('CountController', CountController);

function CountController($scope, $controller, $filter, PostService){  
  $scope.heatmapControl = {};
  $scope.dataLoadedPercent = 100;

  $scope.loadDataForVisualization = function(){
    $scope.dataLoadedPercent = 0;
    PostService.count(function(data){
      $scope.computeLocationData(data.locationMap);

      $scope.loadHeatPoints();
      $scope.constructChartObject();

      $scope.computeDateData(data.dateMap);
      $scope.loadTimeChartData();
      
      $scope.computeHourData(data.hourMap);
      $scope.dataLoadedPercent = data.percentLoaded;
    })
  }

  $scope.loadDataForVisualization();

  angular.extend(this, $controller('AbstractLocationDataController', {$scope: $scope}));
  angular.extend(this, $controller('AbstractChartController', {$scope: $scope}));
  angular.extend(this, $controller('AbstractTimelineController', {$scope: $scope}));
  angular.extend(this, $controller('AbstractMapController', {$scope: $scope}));
  

  $scope.dateData = [];
  $scope.hoursData = [];

  $scope.computeHourData = function(data){
    for(var key in data) { 
      if(data.hasOwnProperty(key)) { 
        var hoursDataKey = key.split(":")[0];
        if($scope.hoursData[hoursDataKey]==undefined){
          $scope.hoursData[hoursDataKey] = {};
          $scope.hoursData[hoursDataKey].hour = hoursDataKey;
          $scope.hoursData[hoursDataKey].count = data[key].count;
        }else{
          $scope.hoursData[hoursDataKey].count = $scope.hoursData[hoursDataKey].count + data[key].count;
        }
      } 
    }
    console.log($scope.hoursData);
  }

}