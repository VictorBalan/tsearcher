angular.module('tsearcher.directives').directive('navbar', function() {
	return {
	    restrict: 'E',
	    templateUrl: 'scripts/shared/navbar/navbar.html',
	    controller:  function($scope) {
	        $scope.items = [];
	        $scope.constructMenu = function(){
        		$scope.brand = "TSearcher";
    			$scope.items.push({href: "#/count", name: "Count"});
    			$scope.items.push({href: "#/search", name: "Search"});
    			$scope.items.push({href: "#/people", name: "People"});

	        }
	        $scope.constructMenu();
	    }
	};
});