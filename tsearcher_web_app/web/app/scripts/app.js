'use strict';

/**
 * @ngdoc overview
 * @name webApp
 * @description
 * # webApp
 *
 * Main module of the application.
 */
angular
.module('webApp', [
  'ngAria',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'tsearcher.routes',
  'tsearcher.services',
  'tsearcher.controllers',
  'tsearcher.directives',
  'uiGmapgoogle-maps',
  'googlechart'
])
.config(function ($routeProvider, uiGmapGoogleMapApiProvider) {
  uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyAMxIg_C7M-lRLL01j0rBRnmTJaD2Z3P0w',
      v: '3.17',
      libraries: 'weather,geometry,visualization'
  });
  $routeProvider
    .when('/', {
      templateUrl: 'scripts/shared/general/main.html'
    })
    .otherwise({
      redirectTo: '/'
    });
});
angular.module('tsearcher.routes',[]);
angular.module('tsearcher.services',[]);
angular.module('tsearcher.controllers',[]);
angular.module('tsearcher.directives',[])
