import socket               # Import socket module
import rake
import struct
import operator
import thread

def recv_one_message(sock):
    language = recvall(sock,1)
    lengthbuf = recvall(sock, 4)
	
    length, = struct.unpack('!I', lengthbuf)
    return recvall(sock, length), language
	
def recvall(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
	
def processText(text, language):
	if(language=='r'):
		stoppath = "stop_list_ro.txt"
	else:
		stoppath = "stop_list_en.txt"
	
	# EXAMPLE ONE - SIMPLE

	# 1. initialize RAKE by providing a path to a stopwords file
	rake_object = rake.Rake(stoppath, 5, 3, 4)
	sentenceList = rake.split_sentences(text)
	for sentence in sentenceList:
		keywords = rake_object.run(text)
		if not keywords:
		
			# generate candidate keywords
			stopwordpattern = rake.build_stop_word_regex(stoppath)
			phraseList = rake.generate_candidate_keywords(sentenceList, stopwordpattern)

			# calculate individual word scores
			wordscores = rake.calculate_word_scores(phraseList)

			# generate candidate keyword scores
			keywordcandidates = rake.generate_candidate_keyword_scores(phraseList, wordscores)

			# sort candidates by score to determine top-scoring keywords
			sortedKeywords = sorted(keywordcandidates.iteritems(), key=operator.itemgetter(1), reverse=True)
			totalKeywords = len(sortedKeywords)

			return sortedKeywords;
		else:	
			return keywords;

def threadHandler(client, addr):		
	text, language =  recv_one_message(client);

	client.send(str(processText(text, language)))
	client.close()                # Close the connection
	
if __name__ == '__main__':	
	sock = socket.socket()         # Create a socket object
	host = '127.0.0.1';#socket.gethostname() # Get local machine name
	port = 12345                # Reserve a port for your service.
	sock.bind((host, port))        # Bind to the port

	sock.listen(20)                 # Now wait for client connection.

	while True:
		client, addr = sock.accept()     # Establish connection with client.
		print 'Got connection from', addr
		
		thread.start_new_thread(threadHandler, (client, addr));