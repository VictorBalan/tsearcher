package model;

/**
 * Created by Victor on 5/17/2015.
 */
public class KeyPhrase {
    private String phrase;
    private Double grade;

    public KeyPhrase(String phrase, Double grade) {
        this.grade = grade;
        this.phrase = phrase;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }
}
