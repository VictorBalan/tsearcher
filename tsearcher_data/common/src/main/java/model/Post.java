package model;

import com.marklogic.client.pojo.annotation.Id;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Catalysts
 * Date: 07.01.2015
 * Time: 14:27
 * To change this template use File | Settings | File Templates.
 */
public class Post {
    private String id;
    private Long postId;
    private String userScreenName;
    private Long userId;
    private String content;
    private Boolean deleted;
    private Date createdAt;
    private String unprocessedLocation;
    private String processedLocation;
    private String lang;
    private Integer favouriteCount;
    private Integer retweetCount;
    private Boolean isRetweet;
    private GeoLocation geoLocation;

    public Post() {
        this.deleted = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTwitterPostId() {
        return postId;
    }

    public void setTwitterPostId(Long postId) {
        this.postId = postId;
    }

    public String getUserScreenName() {
        return userScreenName;
    }

    public void setUserScreenName(String userScreenName) {
        this.userScreenName = userScreenName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getUnprocessedLocation() {
        return unprocessedLocation;
    }

    public void setUnprocessedLocation(String unprocessedLocation) {
        this.unprocessedLocation = unprocessedLocation;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Integer getFavouriteCount() {
        return favouriteCount;
    }

    public void setFavouriteCount(Integer favouriteCount) {
        this.favouriteCount = favouriteCount;
    }

    public Integer getRetweetCount() {
        return retweetCount;
    }

    public void setRetweetCount(Integer retweetCount) {
        this.retweetCount = retweetCount;
    }

    public Boolean isRetweet() {
        return isRetweet;
    }

    public void setRetweet(Boolean retweet) {
        this.isRetweet = retweet;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public String getProcessedLocation() {
        return processedLocation;
    }

    public void setProcessedLocation(String processedLocation) {
        this.processedLocation = processedLocation;
    }
}
