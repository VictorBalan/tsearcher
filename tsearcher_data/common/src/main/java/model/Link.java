package model;

import java.util.List;

/**
 * Created by Victor on 5/18/2015.
 */
public class Link {
    private String id;
    private String link;
    private String extendedLink;
    private String content;
    private String language;
    private List<KeyPhrase> keyPhrases;
    private String postId;
    private Boolean processed;

    public Link(String id, String postId, String link, String extendedLink, String content, String language, List<KeyPhrase> keyPhrases) {
        this.id = id;
        this.link = link;
        this.extendedLink = extendedLink;
        this.content = content;
        this.language = language;
        this.keyPhrases = keyPhrases;
        this.postId = postId;
        this.processed = true;
    }

    public Link(String id, String postId, String link, String extendedLink) {
        this.id = id;
        this.link = link;
        this.extendedLink = extendedLink;
        this.postId = postId;
        this.processed = true;
        this.processed = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<KeyPhrase> getKeyPhrases() {
        return keyPhrases;
    }

    public void setKeyPhrases(List<KeyPhrase> keyPhrases) {
        this.keyPhrases = keyPhrases;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public Boolean isProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public String getExtendedLink() {
        return extendedLink;
    }

    public void setExtendedLink(String extendedLink) {
        this.extendedLink = extendedLink;
    }
}
