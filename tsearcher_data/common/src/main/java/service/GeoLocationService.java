package service;


import mapper.CityJSONMapper;
import model.City;
import model.GeoLocation;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import utils.CityUtils;
import utils.GoogleUtils;
import utils.StringUtils;
import utils.UnprocessedCities;

import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Victor on 03/03/2015.
 */
//TODO move in location maven module
public class GeoLocationService {
    private final double northeastLat = 48.265274;
    private final double northeastLng = 29.75710149999999;
    private final double southwestLat = 43.62329769999999;
    private final double southwestLng = 20.2617592;

    private CityJSONMapper cityJSONMapper;
    private HashMap<String, City> cities;

    public GeoLocationService() {
        this.cityJSONMapper = new CityJSONMapper();
        cities = readAllCitiesWithGeoLocationFromFile();
    }

    public void saveAllCitiesWithGeoLocation() {
        JSONArray jsonArray = new JSONArray();
        List<City> cities = findAllCitiesWithGeoLocation();
        for (City city : cities) {
            jsonArray.add(cityJSONMapper.map(city));
        }
        jsonArray.add(cityJSONMapper.map(UnprocessedCities.ALBA));
        jsonArray.add(cityJSONMapper.map(UnprocessedCities.SLATINA));
        try {
            JSONObject toWrite = new JSONObject();
            toWrite.put("cities", jsonArray);
            PrintWriter writer = new PrintWriter("./romaniaProcessedCities.json", "UTF-8");
            writer.println(toWrite.toJSONString());
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GeoLocation findGeoLocationByCity(String city) {
        city = StringUtils.normalizeString(city);
        if (cities.get(city) != null) {
            return cities.get(city).getGeoLocation();
        }
        return null;
    }

    public GeoLocation findGeoLocationByAddress(String address) {
        try {
            URL url = new URL(getUrl(address));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
            String line = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return getGeoLocationFromResponseJsonString(stringBuilder.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isInRomania(double lat, double lng) {
        return lat < northeastLat && lat > southwestLat && lng < northeastLng && lng > southwestLng;
    }

    public boolean isInRomania(GeoLocation geoLocation) {
        if (geoLocation == null || geoLocation.getLat() == null || geoLocation.getLng() == null) {
            return false;
        }
        return isInRomania(geoLocation.getLat(), geoLocation.getLng());
    }

    private HashMap<String, City> readAllCitiesWithGeoLocationFromFile() {
        HashMap<String, City> citiesMap = new HashMap<String, City>();

        JSONParser jsonParser = new JSONParser();
        try {

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("./romaniaProcessedCities.json")),"UTF-8"));
            String content = bufferedReader.readLine();
            Object object = jsonParser.parse(content);

            JSONObject fromFile = (JSONObject) object;
            JSONArray cities = (JSONArray) fromFile.get("cities");
            for (int i = 0; i < cities.size(); i++) {
                City city = cityJSONMapper.map((JSONObject) cities.get(i));
                citiesMap.put(StringUtils.normalizeString(city.getName()), city);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return citiesMap;
    }

    private List<City> findAllCitiesWithGeoLocation() {
        List<City> cityList = new ArrayList<City>();
        String[] cities = CityUtils.getRomaniaCities();
        for (String city : cities) {
            GeoLocation geoLocation = findGeoLocationByAddress(city);
            if (geoLocation != null) {
                if (isInRomania(geoLocation)) {
                    cityList.add(new City(city, geoLocation));
                } else {
                    System.out.println(city);
                }
            }
        }
        return cityList;
    }

    private GeoLocation getGeoLocationFromResponseJsonString(String string) throws ParseException {
        if (string != null) {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(string);

            JSONArray resultsArray = (JSONArray) jsonObject.get("results");
            if (resultsArray.size() > 0) {
                JSONObject results = (JSONObject) resultsArray.get(0);
                JSONObject geometry = (JSONObject) results.get("geometry");
                JSONObject location = (JSONObject) geometry.get("location");

                Double lat = Double.parseDouble(String.valueOf(location.get("lat")));
                Double lng = Double.parseDouble(String.valueOf(location.get("lng")));

                return new GeoLocation(lat, lng);
            }
        }
        return null;
    }

    private String getUrl(String address) throws Exception {
        String addressParam = String.format("address=%s", URLEncoder.encode(address, "UTF-8"));
        String keyParam = String.format("key=%s", GoogleUtils.GOOGLE_API_KEY);

        String params = String.format("%s&%s", addressParam, keyParam);

        return GoogleUtils.GEOLOCATION_URL + params;
    }
}
