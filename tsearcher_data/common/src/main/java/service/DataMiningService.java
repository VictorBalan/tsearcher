package service;//import model.Post;

import com.cybozu.labs.langdetect.LangDetectException;
import mapper.LinkJSONMapper;
import model.KeyPhrase;
import model.Link;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import utils.LangDetect;
import utils.URLUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: Victor
 * Date: 08/01/15
 * Time: 19:52
 * To change this template use File | Settings | File Templates.
 */
public class DataMiningService {
    private ExecutorService executorService = Executors.newFixedThreadPool(2);
    private LinkJSONMapper linkMapper;
    private LinkService linkService;

    public DataMiningService(LinkService linkService) {
        linkMapper = new LinkJSONMapper();
        this.linkService = linkService;
    }

    private static List<KeyPhrase> processLine(String line) {
        line = line.replace("[", "");
        line = line.replace("]", "");
        line = line.replace("(", "");
        line = line.replace(")", "");
        line = line.replace("'", "");
        if (line.length() > 0) {
            String[] splitLine = line.split(",");
            List<KeyPhrase> keyPhrases = new ArrayList<KeyPhrase>();
            for (int i = 0; i < splitLine.length; i += 2) {
                keyPhrases.add(new KeyPhrase(splitLine[i], Double.parseDouble(splitLine[i + 1])));
            }
            return keyPhrases;
        } else {
            return null;
        }
    }

    public String getLinkId(String link) {
        String[] splittedLink = link.split("/");
        if(splittedLink.length>3){
            return splittedLink[3];
        }else{
            return "undefined";
        }
    }

    public void processPost(String post, final String postId) {
        List<String> links = URLUtils.getUrls(post);
        if (links != null) {
            for (final String url : links) {
                try {
                    final String extendedUrl = URLUtils.getExpandedUrl(url);
                    try {
                        final String siteText = startMining(extendedUrl);
//                        executorService.execute(new Runnable() {
//                            public void run() {
                                try {
                                    String lang = LangDetect.detect(siteText);

                                    if (lang != null && lang.compareTo("en") == 0) {
                                        if (lang == null || (lang.compareTo("ro") != 0 && lang.compareTo("en") != 0)) {
                                            lang = "en";
                                        }
                                        List<KeyPhrase> keyPhrases = processWithRake(siteText, lang);
                                        Link link = new Link(getLinkId(url), postId, url, extendedUrl, siteText, lang, keyPhrases);
                                        linkService.saveLink(link);
                                    }
                                } catch (LangDetectException e) {
                                    Link link = new Link(getLinkId(url), postId, url, extendedUrl);
                                    linkService.saveLink(link);
                                }
//                            }
//                        });
                    } catch (Exception e) {
                        Link link = new Link(getLinkId(url), postId, url, extendedUrl);
                        linkService.saveLink(link);
                    }
                } catch (IOException e) {
                    Link link = new Link(getLinkId(url), postId, url, null);
                    linkService.saveLink(link);
                }
            }
        }
    }

    public void processLink(final String url, final String postId){
        try {
            final String extendedUrl = URLUtils.getExpandedUrl(url);
            try {
                final String siteText = startMining(extendedUrl);
                executorService.execute(new Runnable() {
                    public void run() {
                        try {
                            String lang = LangDetect.detect(siteText);

                            if (lang != null && lang.compareTo("en") == 0) {
                                if (lang == null || (lang.compareTo("ro") != 0 && lang.compareTo("en") != 0)) {
                                    lang = "en";
                                }
                                List<KeyPhrase> keyPhrases = processWithRake(siteText, lang);
                                Link link = new Link(getLinkId(url), postId, url, extendedUrl, siteText, lang, keyPhrases);
                                linkService.saveLink(link);
                            }
                        } catch (LangDetectException e) {
                            Link link = new Link(getLinkId(url), postId, url, extendedUrl);
                            linkService.saveLink(link);
                        }
                    }
                });
            } catch (Exception e) {
                Link link = new Link(getLinkId(url), postId, url, extendedUrl);
                linkService.saveLink(link);
            }
        } catch (IOException e) {
            Link link = new Link(getLinkId(url), postId, url, null);
            linkService.saveLink(link);
        }
    }

    public String startMining(String link) throws Exception {
        Document doc = Jsoup.connect(link).get();
        return doc.text();
    }

    public List<KeyPhrase> processWithRake(String text, String lang) {
        if (lang.compareTo("ro") == 0) {
//            text = normalizeString(text);
        }
        char langToSend = lang.charAt(0);
        try {
            Socket socket = new Socket(InetAddress.getByName("127.0.0.1"), 12345);


            ByteBuffer bytes = ByteBuffer.allocate(4);
            bytes.putInt(text.getBytes().length);
            byte[] buffer = bytes.array();

            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(langToSend);
            outputStream.write(buffer);
            outputStream.write(text.getBytes());
            outputStream.flush();


            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line = "";
            List<KeyPhrase> keyPhrases = new ArrayList<KeyPhrase>();
            while ((line = bufferedReader.readLine()) != null) {
                List<KeyPhrase> processedKeywords = processLine(line);
                if (processedKeywords != null) {
                    keyPhrases.addAll(processedKeywords);
                }
            }
            outputStream.close();
            bufferedReader.close();
            socket.close();
            return keyPhrases;
        } catch (Exception e) {
            System.err.println("RAKE connection error.");
            return null;
        }
    }

    /**
     * @param string to normalize
     * @return the same string without diacritics or apostrophes
     */
    private String normalizeString(String string) {
        //string = Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        return StringUtils.stripAccents(string).toLowerCase();
    }
}
