package service;

import com.mongodb.DB;
import mapper.CityMapper;
import model.City;
import repository.CityRepository;

/**
 * Created by Victor on 03/03/2015.
 */
public class CityService {
    private CityRepository cityRepository;
    private CityMapper cityMapper;

    public CityService(DB database) {
        this.cityRepository = new CityRepository(database);
        this.cityMapper = new CityMapper();
    }

    public void save(City city) {
        cityRepository.save(cityMapper.map(city));
    }
}
