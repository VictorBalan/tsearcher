package service;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.StringHandle;
import mapper.PostMapper;
import model.Post;
import utils.MarkLogicUtils;

/**
 * Created by Victor on 4/23/2015.
 */
public class MarkLogicService {
    DatabaseClient databaseClient;
    JSONDocumentManager docMgr;
    private PostMapper postMapper;
    private DocumentMetadataHandle postMetaData;
    private DocumentMetadataHandle linkMetaData;

    public MarkLogicService() {
        this.databaseClient = MarkLogicUtils.newDatabaseClient(DatabaseClientFactory.Authentication.DIGEST);
        this.docMgr = databaseClient.newJSONDocumentManager();
        this.postMapper = new PostMapper();
        this.postMetaData = new DocumentMetadataHandle();
        this.postMetaData.getCollections().addAll("twitter", "post");
    }

    public void savePost(Post post) {
        if (post.getId() == null || post.getId().compareTo("") == 0) {
            System.out.println("Post id cannot be null. MARKLOGICSERVICE");
            System.exit(23);
        }
        String toWrite = postMapper.mapToJSON(post).toJSONString();
        docMgr.write(String.valueOf(post.getId()), postMetaData, new StringHandle(toWrite));
    }

    public void saveLink(Post post) {
        if (post.getId() == null || post.getId().compareTo("") == 0) {
            System.out.println("Post id cannot be null. MARKLOGICSERVICE");
            System.exit(23);
        }
        String toWrite = postMapper.mapToJSON(post).toJSONString();
        docMgr.write(String.valueOf(post.getId()), postMetaData, new StringHandle(toWrite));
    }
}
