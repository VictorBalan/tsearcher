package service;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.admin.config.QueryOptions;
import com.marklogic.client.admin.config.QueryOptionsBuilder;
import com.marklogic.client.document.DocumentPage;
import com.marklogic.client.document.DocumentRecord;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.QueryOptionsHandle;
import com.marklogic.client.io.StringHandle;
import com.marklogic.client.io.marker.JSONReadHandle;
import com.marklogic.client.pojo.PojoPage;
import com.marklogic.client.pojo.PojoQueryBuilder;
import com.marklogic.client.pojo.PojoQueryDefinition;
import com.marklogic.client.pojo.PojoRepository;
import com.marklogic.client.query.*;
import com.mongodb.QueryBuilder;
import mapper.PostMapper;
import model.GeoLocation;
import model.Post;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import twitter4j.Status;
import utils.CustomStringUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Victor on 02/03/2015.
 */
public class PostService extends AbstractMarklogicService{
    private PostMapper postMapper;
    private LocationService locationService;
    private DataMiningService dataMiningService;
    private ExecutorService executorService = Executors.newFixedThreadPool(100);

    //Marklogic
    public PostService(DatabaseClient databaseClient, LinkService linkService) {
        super(databaseClient);
        this.postMapper = new PostMapper();
        this.locationService = new LocationService();
        this.dataMiningService = new DataMiningService(linkService);
    }

    @Override
    DocumentMetadataHandle initializeMetadata() {
        DocumentMetadataHandle metadata = new DocumentMetadataHandle();
        metadata.getCollections().addAll("twitter", "post");
        return metadata;
    }

    public void save(Post post) {
        post.setProcessedLocation(getProcessedLocation(post.getUnprocessedLocation()));

        jsonDocumentManager.write(
                String.valueOf(post.getId()),
                getMetadata(),
                new StringHandle(postMapper.mapToJSON(post).toJSONString()));

//        executorService.execute(new ProcessPostLinksTask(post.getContent(), String.valueOf(post.getId())));
    }

    public void update(Post post){
        jsonDocumentManager.write(
                String.valueOf(post.getId()),
                getMetadata(),
                new StringHandle(postMapper.mapToJSON(post).toJSONString()));
    }

    public void update(String uri, JSONObject post){
        jsonDocumentManager.write(
                uri,
                getMetadata(),
                new StringHandle(post.toJSONString()));
    }

    public DocumentPage findByOffset(long pageNo, long resultsOnPage){
        QueryOptions.QuerySortOrder sortOrder = new QueryOptions.QuerySortOrder();
        sortOrder.setField(new QueryOptions.Field("username"));
        sortOrder.setDirection(QueryOptions.QuerySortOrder.Direction.ASCENDING);
        QueryOptionsHandle optionsHandle = new QueryOptionsHandle().withSortOrders();
        jsonDocumentManager.setPageLength(resultsOnPage);

        StructuredQueryBuilder qb = queryManager.newStructuredQueryBuilder();
        StructuredQueryDefinition query = qb.collection("post");

        DocumentPage documentRecords = jsonDocumentManager.search(query, pageNo*resultsOnPage);

        return documentRecords;
    }

    public long count(){
        StructuredQueryBuilder qb = queryManager.newStructuredQueryBuilder();

        StructuredQueryDefinition query = qb.collection("post");
        DocumentPage documentRecords = jsonDocumentManager.search(query, 0);
        return documentRecords.getTotalSize();
    }


    /**
     * @param status the twitter status to be mapped and saved as a Post
     * @return true if the status was from Romania.
     */
    public boolean saveWithGeoLocation(Status status) {
        if (CustomStringUtils.checkForStringMatch(status.getLang(), "en", "ro", "und")) {
            GeoLocation geoLocation = locationService.getGeoLocation(status);
            if (geoLocation != null) {
                System.out.println("@" + status.getUser().getScreenName() + " - " + status.getLang()
                        + " - " + status.getUser().getLocation()
                        + " - " + geoLocation.getLat() + " " + geoLocation.getLng() + " - " + status.getText());
                Post post = postMapper.map(status);
                post.setGeoLocation(geoLocation);
                save(post);
                return true;
            } else if (locationService.isLocationRomania(status)) {
                System.out.println("@" + status.getUser().getScreenName() + " - " + status.getLang()
                        + " - " + status.getUser().getLocation() + " - " + status.getText());
                save(postMapper.map(status));
                return true;
            }
        }
        return false;
    }

    //TODO move this part in location service rack function
    private String getProcessedLocation(String unprocessedLocation) {
        String processedLocation = locationService.getProcessedLocation(unprocessedLocation);
        if (processedLocation == null) {
            if (locationService.cityCheck(unprocessedLocation, "romania")) {
                processedLocation = "Romania";
            }
        } else {
            if ("Bucharest".compareTo(processedLocation) == 0) {
                processedLocation = "Bucureşti";
            }
            if ("Cluj".compareTo(processedLocation) == 0) {
                processedLocation = "Cluj-Napoca";
            }
        }
        return processedLocation;
    }

    private class ProcessPostLinksTask implements Runnable{
        private String post;
        private String postId;

        public ProcessPostLinksTask(String post, String postId) {
            this.post = post;
            this.postId = postId;
        }

        public void run() {
            dataMiningService.processPost(post, postId);
        }
    }
}
