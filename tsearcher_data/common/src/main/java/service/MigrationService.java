package service;

import com.marklogic.client.document.DocumentPage;
import com.marklogic.client.document.DocumentRecord;
import com.marklogic.client.io.StringHandle;
import mapper.PostMapper;
import model.GeoLocation;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import utils.LimitedQueue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by Victor on 5/25/2015.
 */
public class MigrationService {
    private PostService postService;
    private LocationService locationService;
    private GeoLocationService geoLocationService;
    private DataMiningService dataMiningService;
//    private ExecutorService executorService = Executors.newFixedThreadPool(1);
    private ExecutorService executorService = new ThreadPoolExecutor(100, 100,
                           5000L, TimeUnit.MILLISECONDS,
                           new ArrayBlockingQueue<Runnable>(1000), new ThreadPoolExecutor.CallerRunsPolicy());

    public MigrationService(PostService postService, LinkService linkService) {
        this.postService = postService;
        this.locationService = new LocationService();
        this.geoLocationService = new GeoLocationService();
        this.dataMiningService = new DataMiningService(linkService);
    }

    public void startPostLinkProcessing() {
        long totalSize = postService.count();
        long resultsOnPage = 5000;
        long totalPages = totalSize / resultsOnPage;

        for (long i = 1; i <= totalPages; i++) {
            System.out.println(String.format("Page %s of %s", i, totalPages));
            final DocumentPage documentRecords = postService.findByOffset(i, resultsOnPage);
            Iterator<DocumentRecord> iterator = documentRecords.iterator();
            long count = 1;
            while (iterator.hasNext()) {
                count++;
                final DocumentRecord documentRecord = iterator.next();
                StringHandle content = documentRecord.getContent(new StringHandle());
                executorService.execute(new LinkProcessingThread(content.toString(),(i-1)*resultsOnPage +  count));
            }
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class LinkProcessingThread implements Runnable {
        private String content;
        private Long counter;
        public LinkProcessingThread(String content, Long counter) {
            this.content = content;
            this.counter = counter;
        }

        public void run() {
            try {
                System.out.println(counter);
                JSONParser jsonParser = new JSONParser();
                JSONObject jsonObject = (JSONObject) jsonParser.parse(content);
                dataMiningService.processPost(jsonObject.get(PostMapper.CONTENT).toString(),
                        jsonObject.get(PostMapper.POST_ID).toString());
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public void startDateFormatChangeMigration() {
        long totalSize = postService.count();
        long resultsOnPage = 5000;
        long totalPages = totalSize / resultsOnPage;

        JSONParser jsonParser = new JSONParser();
        for (long i = 1; i <= totalPages; i++) {
            System.out.println(String.format("Page %s of %s", i, totalPages));
            DocumentPage documentRecords = postService.findByOffset(i, resultsOnPage);
            Iterator<DocumentRecord> iterator = documentRecords.iterator();
            boolean first = true;
            while (iterator.hasNext()) {
                DocumentRecord documentRecord = iterator.next();
                StringHandle content = documentRecord.getContent(new StringHandle());
                try {
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(content.toString());
                    if (first) {
                        System.out.println(jsonObject.toJSONString());
                        first = false;
                    }
                    jsonObject.put("postId", String.valueOf(documentRecord.getUri()));
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzzz yyyy");
                    try {
                        Date date = simpleDateFormat.parse(jsonObject.get(PostMapper.CREATED_AT).toString());
                        simpleDateFormat.applyPattern("yyyy-MM-dd");
                        jsonObject.put(PostMapper.CREATED_AT, simpleDateFormat.format(date));
                    } catch (java.text.ParseException e) {

                    }
                    postService.update(documentRecord.getUri(), jsonObject);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void startDateFormatChangeMigrationCheck() {
        long totalSize = postService.count();
        long resultsOnPage = 5000;
        long totalPages = totalSize / resultsOnPage;
        int errorCount = 0;

        JSONParser jsonParser = new JSONParser();
        for (long i = 1; i <= totalPages + 1; i++) {
            System.out.println(String.format("Page %s of %s", i, totalPages));
            DocumentPage documentRecords = postService.findByOffset(i, resultsOnPage);
            Iterator<DocumentRecord> iterator = documentRecords.iterator();
            boolean firstElem = true;
            while (iterator.hasNext()) {
                DocumentRecord documentRecord = iterator.next();
                StringHandle content = documentRecord.getContent(new StringHandle());
                try {
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(content.toString());
                    if (jsonObject.get("postId").toString().compareTo("525506654973231104") == 0) {
                        int x = 2;
                    }
                    if (firstElem) {
                        System.out.println(jsonObject.get("postId").toString());
                        firstElem = false;
                    }
//                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzzz yyyy");
                    try {
                        Date date = simpleDateFormat.parse(jsonObject.get(PostMapper.CREATED_AT).toString());
                        System.out.println(jsonObject.get(PostMapper.CREATED_AT).toString());
                        errorCount++;
                    } catch (java.text.ParseException e) {
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
        System.out.println(errorCount);
    }

    public void startIdMigration() {
        long totalSize = postService.count();
        long resultsOnPage = 5000;
        long totalPages = totalSize / resultsOnPage;
        totalPages++;

        JSONParser jsonParser = new JSONParser();
        for (long i = 0; i <= totalPages; i++) {
            System.out.println(String.format("Page %s of %s", i, totalPages));
            DocumentPage documentRecords = postService.findByOffset(i, resultsOnPage);
            Iterator<DocumentRecord> iterator = documentRecords.iterator();
            while (iterator.hasNext()) {
                DocumentRecord documentRecord = iterator.next();
                StringHandle content = documentRecord.getContent(new StringHandle());
                try {
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(content.toString());
                    jsonObject.put("postId", String.valueOf(documentRecord.getUri()));
                    postService.update(documentRecord.getUri(), jsonObject);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    public void startGeolocationMigration() {
        long totalSize = postService.count();
        long resultsOnPage = 5000;
        long totalPages = totalSize / resultsOnPage;
        totalPages++;

        JSONParser jsonParser = new JSONParser();
        for (long i = 0; i <= totalPages; i++) {
            System.out.println(String.format("Page %s of %s", i, totalPages));
            DocumentPage documentRecords = postService.findByOffset(i, resultsOnPage);
            Iterator<DocumentRecord> iterator = documentRecords.iterator();
            while (iterator.hasNext()) {
                DocumentRecord documentRecord = iterator.next();
                StringHandle content = documentRecord.getContent(new StringHandle());
                try {
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(content.toString());
                    if(Boolean.parseBoolean(String.valueOf(jsonObject.get("hasGeolocation")))==true){
                        JSONObject geo = (JSONObject)jsonObject.get("geolocation");
                        geo.put("lon", geo.get("lng"));
                        jsonObject.put(PostMapper.GEOLOCATION, geo);
                    }
                    postService.update(documentRecord.getUri(), jsonObject);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void checkIdMigration() {
        HashMap<String, Integer> idFrequency = new HashMap<String, Integer>();

        long totalSize = postService.count();
        long resultsOnPage = 5000;
        long totalPages = totalSize / resultsOnPage;
        totalPages++;

        JSONParser jsonParser = new JSONParser();
        for (long i = 0; i <= totalPages; i++) {
            System.out.println(String.format("Page %s of %s", i, totalPages));
            DocumentPage documentRecords = postService.findByOffset(i, resultsOnPage);
            Iterator<DocumentRecord> iterator = documentRecords.iterator();
            while (iterator.hasNext()) {
                DocumentRecord documentRecord = iterator.next();
                StringHandle content = documentRecord.getContent(new StringHandle());
                try {
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(content.toString());
                    String postId = String.valueOf(jsonObject.get("postId"));
                    String uri = documentRecord.getUri();
                    if (idFrequency.get(postId) != null) {
                        idFrequency.put(postId, idFrequency.get(postId) + 1);
                    } else {
                        idFrequency.put(postId, 1);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
        int count = 0;
        for (Map.Entry<String, Integer> entry : idFrequency.entrySet()) {
            if (entry.getValue() > 1) {
                count++;
            }
        }
        System.out.println(count);
    }

    public void startLocationMigration() {
        long totalSize = postService.count();
        long resultsOnPage = 5000;
        long totalPages = totalSize / resultsOnPage;

        JSONParser jsonParser = new JSONParser();
        for (long i = 1; i <= totalPages; i++) {
            System.out.println(String.format("Page %s of %s", i, totalPages));
            DocumentPage documentRecords = postService.findByOffset(1, 100);
            Iterator<DocumentRecord> iterator = documentRecords.iterator();
            while (iterator.hasNext()) {
                DocumentRecord documentRecord = iterator.next();
                StringHandle content = documentRecord.getContent(new StringHandle());
                try {
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(content.toString());
                    String processedLocation = locationService.getProcessedLocation(jsonObject.get(PostMapper.UNPROCESSED_LOCATION).toString());
                    if (processedLocation != null) {
                        jsonObject.put(PostMapper.PROCESSED_LOCATION, processedLocation);
                        GeoLocation geoLocation = geoLocationService.findGeoLocationByCity(processedLocation);
                        if (geoLocation != null) {
                            JSONObject jsonGeoLocation = new JSONObject();
                            jsonGeoLocation.put(PostMapper.LAT, geoLocation.getLat());
                            jsonGeoLocation.put(PostMapper.LNG, geoLocation.getLng());
                            jsonObject.put(PostMapper.GEOLOCATION, jsonGeoLocation);
                            jsonObject.put("hasGeolocation", true);
                        } else {
                            jsonObject.put("hasGeolocation", false);
                        }
                    } else {
                        jsonObject.put(PostMapper.PROCESSED_LOCATION, "Romania");
                    }
                    postService.update(documentRecord.getUri(), jsonObject);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
