package service;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.query.QueryManager;

/**
 * Created by Victor on 5/25/2015.
 */
public abstract class AbstractMarklogicService {
    protected final JSONDocumentManager jsonDocumentManager;
    private final DocumentMetadataHandle serviceMetaData;
    protected final QueryManager queryManager;

    public AbstractMarklogicService(DatabaseClient databaseClient) {
        this.jsonDocumentManager = databaseClient.newJSONDocumentManager();
        this.serviceMetaData = initializeMetadata();
        this.queryManager = databaseClient.newQueryManager();
    }

    abstract DocumentMetadataHandle initializeMetadata();

    public DocumentMetadataHandle getMetadata() {
        return serviceMetaData;
    }
}
