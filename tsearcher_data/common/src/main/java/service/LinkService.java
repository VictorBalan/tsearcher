package service;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.StringHandle;
import mapper.LinkJSONMapper;
import model.Link;
import model.Post;

/**
 * Created by Victor on 5/25/2015.
 */
public class LinkService extends AbstractMarklogicService{
    private LinkJSONMapper linkMapper;

    public LinkService(DatabaseClient databaseClient){
        super(databaseClient);
        this.linkMapper = new LinkJSONMapper();
    }

    @Override
    DocumentMetadataHandle initializeMetadata() {
        DocumentMetadataHandle metadata = new DocumentMetadataHandle();
        metadata.getCollections().addAll("twitter", "link");
        return metadata;
    }

    public void saveLink(Link link){
        if(link.getId().length()>11){
            int x = 2;
        }
        jsonDocumentManager.write(String.valueOf(link.getId()),
                getMetadata(),
                new StringHandle(linkMapper.map(link).toJSONString()));
    }

}
