package service;

import model.GeoLocation;
import org.apache.commons.lang3.StringUtils;
import twitter4j.Status;
import utils.CityUtils;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Victor on 02/03/2015.
 */
//TODO move in location maven module
public class LocationService {
    private static String[] CITIES;
    private final Pattern delimiterPattern = Pattern.compile(" |\\t|,|\\.|;|\\s|-");
    private GeoLocationService geoLocationService;

    public LocationService() {
        CITIES = CityUtils.getRomaniaCities();
        geoLocationService = new GeoLocationService();
    }

    /**
     * @param status to be processed
     * @return geoLocation if status in romania else null
     */
    public GeoLocation getGeoLocation(Status status) {
        GeoLocation geoLocation = null;
        if (status.getGeoLocation() != null) {
            geoLocation = new GeoLocation(
                    status.getGeoLocation().getLatitude(),
                    status.getGeoLocation().getLongitude());
            return geoLocationService.isInRomania(geoLocation) ? geoLocation : null;
        }
        if (status.getPlace() != null) {
            if (status.getPlace().getStreetAddress() != null) {
                geoLocation = geoLocationService.findGeoLocationByAddress(status.getPlace().getStreetAddress());
                return geoLocationService.isInRomania(geoLocation) ? geoLocation : null;
            }
            if (status.getPlace().getGeometryCoordinates() != null) {
                double lat = status.getPlace().getGeometryCoordinates()[0][0].getLatitude();
                double lng = status.getPlace().getGeometryCoordinates()[0][0].getLongitude();
                geoLocation = new GeoLocation(lat, lng);
                return geoLocationService.isInRomania(geoLocation) ? geoLocation : null;
            }
        }
        if (status.getUser().getLocation() != null && "".compareTo(status.getUser().getLocation()) != 0) {
            String city = getProcessedLocation(status.getUser().getLocation());
            if (city != null) {
                geoLocation = geoLocationService.findGeoLocationByCity(city);
                return geoLocationService.isInRomania(geoLocation) ? geoLocation : null;
            }
        }
        return null;
    }

    public boolean isLocationRomania(Status status) {
        if (status.getPlace() != null
                && status.getPlace().getCountryCode() != null
                && status.getPlace().getCountryCode().toLowerCase().compareTo("ro") == 0) {
            return true;
        }
        if (cityCheck(status.getUser().getLocation(), "romania")) {
            return true;
        }
        return false;
    }

    public String getProcessedLocation(String city) {
        if (city == null) {
            return null;
        }

        //TODO must see how to avoid corrupted data(there may exist). ex: The dublin instutute Arad. It will match for Arad
        for (String toCheck : CITIES) {
            if (cityCheck(city, toCheck)) {
                return toCheck;
            }
        }
        return null;
    }

    /**
     * @param contains    user location to be checked
     * @param isContained an existing city
     * @return true if user location contains the isContained city
     */
    public boolean cityCheck(String contains, String isContained) {
        contains = utils.StringUtils.normalizeString(contains);
        isContained = utils.StringUtils.normalizeString(isContained);
        int index = contains.indexOf(isContained);
        if (index != -1) {
            boolean ok = true;
            if (index != 0) {
                Matcher matcher = delimiterPattern.matcher(String.valueOf(contains.charAt(index - 1)));
                ok &= matcher.matches();
            }
            if ((index + isContained.length()) < contains.length()) {
                Matcher matcher = delimiterPattern.matcher(String.valueOf(contains.charAt(index + isContained.length())));
                ok &= matcher.matches();
            }
            return ok;
        }
        return false;
    }

}
