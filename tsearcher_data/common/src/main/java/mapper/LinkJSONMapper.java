package mapper;

import model.KeyPhrase;
import model.Link;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created by Victor on 5/25/2015.
 */
public class LinkJSONMapper {
    private static final String ID = "id";
    private static final String LINK = "link";
    private static final String EXTENDED_LINK = "extendedLink";
    private static final String CONTENT = "content";
    private static final String POST_ID = "postId";
    private static final String KEY_PHRASES = "keyPhrases";
    private static final String KEY = "key";
    private static final String VALUE = "value";
    private static final String PROCESSED = "processed";
    private static final String LANGUAGE = "language";


    public JSONObject map(Link link){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ID, link.getId());
        jsonObject.put(LINK, link.getLink());
        jsonObject.put(POST_ID, link.getPostId());
        jsonObject.put(PROCESSED, link.isProcessed());
        jsonObject.put(EXTENDED_LINK, link.getExtendedLink());
        if(link.isProcessed()) {
            jsonObject.put(CONTENT, link.getContent());
            jsonObject.put(KEY_PHRASES, getKeyPhrases(link));
            jsonObject.put(LANGUAGE, link.getLanguage());
        }

        return jsonObject;
    }

    private JSONArray getKeyPhrases(Link link){
        JSONArray jsonArray = new JSONArray();
        for(KeyPhrase keyPhrase:link.getKeyPhrases()) {
            jsonArray.add(createKeyPhrase(keyPhrase));
        }
        return jsonArray;
    }

    private JSONObject createKeyPhrase(KeyPhrase keyPhrase){
        JSONObject jsonObject = new JSONObject();

        jsonObject.put(KEY, keyPhrase.getPhrase());
        jsonObject.put(VALUE, keyPhrase.getGrade());

        return jsonObject;
    }
}
