package mapper;

import com.mongodb.BasicDBObject;
import model.GeoLocation;
import model.Post;
import org.json.simple.JSONObject;
import twitter4j.Status;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Victor on 02/03/2015.
 */
public class PostMapper {
    public static final String UNPROCESSED_LOCATION = "unprocessedLocation";
    public static final String PROCESSED_LOCATION = "processedLocation";
    public static final String FAVOURITE_COUNT = "favouriteCount";
    private static final String ID = "_id";
    public static final String POST_ID = "postId";
    public static final String CONTENT = "content";
    private static final String DELETED = "deleted";
    private static final String USER_ID = "userId";
    public static final String CREATED_AT = "createdAt";
    private static final String LANG = "lang";
    private static final String RETWEET_COUNT = "retweetCount";
    private static final String IS_RETWEET = "isRetweet";
    private static final String USERNAME = "username";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String GEOLOCATION = "geolocation";

    public Post map(Status status) {
        Post post = new Post();

        post.setId(String.valueOf(status.getId()));
        post.setTwitterPostId(status.getId());
        post.setUserId(status.getUser().getId());
        post.setUserScreenName(status.getUser().getScreenName());
        post.setContent(status.getText());
        post.setCreatedAt(status.getCreatedAt());
        post.setUnprocessedLocation(status.getUser().getLocation());
        post.setFavouriteCount(status.getFavoriteCount());
        post.setRetweetCount(status.getRetweetCount());
        post.setRetweet(status.isRetweet());
        post.setLang(status.getLang());

        return post;
    }

    public JSONObject mapToJSON(Post post) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(POST_ID, String.valueOf(post.getTwitterPostId()));
        jsonObject.put(CONTENT, post.getContent());
        jsonObject.put(DELETED, post.getDeleted());
        jsonObject.put(USER_ID, post.getUserId());
        jsonObject.put(USERNAME, post.getUserScreenName());
        jsonObject.put(CREATED_AT, mapDate(post.getCreatedAt()));
        jsonObject.put(UNPROCESSED_LOCATION, post.getUnprocessedLocation());
        jsonObject.put(LANG, post.getLang());
        jsonObject.put(FAVOURITE_COUNT, post.getFavouriteCount());
        jsonObject.put(RETWEET_COUNT, post.getRetweetCount());
        jsonObject.put(IS_RETWEET, post.isRetweet());
        jsonObject.put(PROCESSED_LOCATION, post.getProcessedLocation());
        if (post.getGeoLocation() != null) {
            JSONObject geoLocation = new JSONObject();
            geoLocation.put(LAT, post.getGeoLocation().getLat());
            geoLocation.put(LNG, post.getGeoLocation().getLng());
            jsonObject.put(GEOLOCATION, geoLocation);
            jsonObject.put("hasGeolocation", true);
        } else {
            jsonObject.put("hasGeolocation", false);
        }
        return jsonObject;
    }

    private GeoLocation mapGeoLocation(BasicDBObject object) {
        if (object == null) {
            return null;
        }
        Double lat = object.getDouble(LAT);
        Double lng = object.getDouble(LNG);
        return lat != null && lng != null ? new GeoLocation(lat, lng) : null;
    }

    private String mapDate(Date date){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZZ");
        String formattedDate = simpleDateFormat.format(date);

        return formattedDate;
    }
}
