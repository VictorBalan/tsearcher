package mapper;

import model.City;
import model.GeoLocation;
import org.json.simple.JSONObject;

/**
 * Created by Victor on 03/03/2015.
 */
public class CityJSONMapper {
    private static final String NAME = "name";
    private static final String LAT = "lat";
    private static final String LNG = "lng";

    public JSONObject map(City city) {
        JSONObject object = new JSONObject();
        object.put(NAME, city.getName());
        object.put(LAT, city.getGeoLocation().getLat());
        object.put(LNG, city.getGeoLocation().getLng());
        return object;
    }

    public City map(JSONObject jsonObject) {
        String cityName = String.valueOf(jsonObject.get(NAME));
        GeoLocation geoLocation = new GeoLocation(getDoubleFromObject(jsonObject.get(LAT)),
                getDoubleFromObject(jsonObject.get(LNG)));
        return new City(cityName, geoLocation);
    }

    private Double getDoubleFromObject(Object obj) {
        return Double.parseDouble(String.valueOf(obj));
    }
}
