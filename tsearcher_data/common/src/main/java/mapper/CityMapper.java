package mapper;

import com.mongodb.BasicDBObject;
import model.City;
import model.GeoLocation;

/**
 * Created by Victor on 03/03/2015.
 */
public class CityMapper {
    private static final String NAME = "name";
    private static final String LAT = "lat";
    private static final String LNG = "lng";

    public BasicDBObject map(City city) {
        BasicDBObject basicDBObject = new BasicDBObject();
        basicDBObject.put(NAME, city.getName());
        basicDBObject.put(LAT, city.getGeoLocation().getLat());
        basicDBObject.put(LNG, city.getGeoLocation().getLng());

        return basicDBObject;
    }

    public City map(BasicDBObject dbObject) {
        String name = dbObject.getString(NAME);
        GeoLocation geoLocation = new GeoLocation(dbObject.getDouble(LAT), dbObject.getDouble(LNG));
        return new City(name, geoLocation);
    }
}
