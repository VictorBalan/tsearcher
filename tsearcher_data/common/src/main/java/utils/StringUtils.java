package utils;

/**
 * Created by Victor on 5/25/2015.
 */
public class StringUtils {
    /**
     * @param string to normalize
     * @return the same string without diacritics or apostrophes
     */
    public static String normalizeString(String string) {
        return org.apache.commons.lang3.StringUtils.stripAccents(string).toLowerCase();
    }
}
