package utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victor on 5/17/2015.
 */
public class URLUtils {

    public static String getExpandedUrl(String urltext) throws IOException {
        return getExpandedUrl(urltext, 0);
    }

    private static String getExpandedUrl(String urltext, Integer steps) throws IOException {
        steps ++;
        URL url = new URL(urltext);

        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection(Proxy.NO_PROXY.NO_PROXY);

        // stop following browser redirect
        httpURLConnection.setInstanceFollowRedirects(false);

        // extract location header containing the actual destination URL
        String expandedURL = httpURLConnection.getHeaderField("Location");

        httpURLConnection.disconnect();
        if(steps>5) {
            return expandedURL == null ? urltext : getExpandedUrl(expandedURL, steps);
        }
        return expandedURL;
    }

    public static List<String> getUrls(String text) {
        if (!text.contains("http")) {
            return null;
        }
        text = org.apache.commons.lang3.StringUtils.stripAccents(text);
        text = text.replaceAll("\"", "");
        text = text.replaceAll("'","");
        text = text.replaceAll("[^A-Za-z0-9()\\[\\]/:.]", " ");
//        text = text.replaceAll("\\p{Cntrl}", "");
        List<String> urls = new ArrayList<String>();
        String[] split = text.split(" |,|\\n|\\t");
        for (String string : split) {
            if (string.contains("t.co/")) {
                if(string.charAt(string.length()-1) == '.'){
                    string = string.substring(0, string.length() - 1);
                }
                urls.add(string);
            }
        }
        return urls;
    }
}
