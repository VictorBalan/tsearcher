package utils;

/**
 * Created by Victor on 02/03/2015.
 */
public class CustomStringUtils {
    public static boolean checkForStringMatch(String toMatch, String... toCheck) {
        if (toMatch == null) {
            return false;
        }
        toMatch = toMatch.toLowerCase();
        for (String string : toCheck) {
            if (string != null && toMatch.compareTo(string.toLowerCase()) == 0) {
                return true;
            }
        }
        return false;
    }
}
