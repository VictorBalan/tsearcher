package utils;

import model.City;
import model.GeoLocation;

/**
 * Created by Victor on 03/03/2015.
 */
//TODO move in location maven module
public class UnprocessedCities {
    public static final City SLATINA = new City("Slatina", new GeoLocation(44.429722, 24.364167));
    public static final City ALBA = new City("Alba", new GeoLocation(46.066944, 23.570000));//same as alba iulia
    /*
    Other unprocessed cities are:
    Roman
    Bocsa
    Rosia
    Campia
    Madaras
    Lacu
    Magura
    Cara
    Tina
    Fabrica
    Filias
    Apata
    Tuzla
    Ocnita
    Prod
    Glina
    Panduri
     */
}
