package utils;

import java.io.*;

/**
 * Created by Victor on 5/30/2015.
 */
public class FileUtils {
    public static String readFromFile(String fileName){
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(fileName)),"UTF-8"));
            StringBuilder stringBuilder = new StringBuilder();
            String line ="";
            while((line=bufferedReader.readLine())!=null){
                stringBuilder.append(line);
                stringBuilder.append("\n'");
            }
            return stringBuilder.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
