package utils;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Victor
 * Date: 07/03/15
 * Time: 14:10
 * To change this template use File | Settings | File Templates.
 */
public class MarkLogicUtils {
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final String HOST = "localhost";
    private static final Integer PORT = 8011;

    public static DatabaseClient newDatabaseClient(DatabaseClientFactory.Authentication authenticationType) {
        return DatabaseClientFactory.newClient(HOST, PORT, USERNAME, PASSWORD, authenticationType);
    }
}
