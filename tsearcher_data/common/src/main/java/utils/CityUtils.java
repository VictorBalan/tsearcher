package utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.util.Locale;

/**
 * Created by Victor on 02/03/2015.
 */
//TODO move in location maven module
public class CityUtils {
    public static String[] getRomaniaCities() {
        JSONParser jsonParser = new JSONParser();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("./romaniaCities.json")),"UTF-8"));
            String content = bufferedReader.readLine();
            Object object = jsonParser.parse(content);
            JSONObject romania = (JSONObject) object;
            JSONArray cities = (JSONArray) romania.get("cities");
            String[] citiesList = new String[cities.size()];
            for (int i = 0; i < cities.size(); i++) {
                citiesList[i] = cities.get(i).toString();
            }
            return citiesList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
