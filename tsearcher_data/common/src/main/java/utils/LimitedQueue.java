package utils;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Victor on 5/31/2015.
 */
public class LimitedQueue<E> extends LinkedBlockingQueue<E>
{
    public LimitedQueue(int maxSize)
    {
        super(maxSize);
    }

    @Override
    public boolean offer(E e)
    {
        // turn offer() and add() into a blocking calls (unless interrupted)
        try {
            put(e);
            return true;
        } catch(InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
        return false;
    }

}