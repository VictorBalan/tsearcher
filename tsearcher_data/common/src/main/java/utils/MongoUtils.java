package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: Victor
 * Date: 04/03/15
 * Time: 13:30
 * To change this template use File | Settings | File Templates.
 */
public class MongoUtils {
    private static final String START_MONGO = "./start_mongo.bat";
    private static final String START_MONGOD = "./start_mongod.bat";
    private static final String MONGO_PROCESS_NAME = "mongo.exe";
    private static final String MONGOD_PROCESS_NAME = "mongod.exe";


    public static void startMongo() throws Exception {
        if (isProcessStarted(MongoUtils.MONGOD_PROCESS_NAME)) {
            System.out.println("mongod.exe is already running.");
        } else {
            runProcess(MongoUtils.START_MONGOD);
            System.out.println("Starting mongod.exe.");
        }
//        if (isProcessStarted(MongoUtils.MONGO_PROCESS_NAME)) {
//            System.out.println("mongo.exe is already running.");
//        } else {
//            runProcess(MongoUtils.START_MONGO);
//            System.out.println("Starting mongo.exe.");
//            Thread.sleep(5000);
//        }
    }

    private static boolean isProcessStarted(String processName) throws IOException {
        boolean started = false;
        String line;
        Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");
        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = input.readLine()) != null) {
            if (line.indexOf(processName) == 0) {
                started = true;
            }
        }
        input.close();
        return started;
    }

    private static void runProcess(String command) throws IOException {
        Runtime.getRuntime().exec(command);
    }
}
