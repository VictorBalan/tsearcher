package repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Victor
 * Date: 07/03/15
 * Time: 13:48
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractMongoRepository {
    private DBCollection table;

    public AbstractMongoRepository(DB database, String tableName) {
        table = database.getCollection(tableName);
    }

    public AbstractMongoRepository(DB database, String tableName, String indexOn) {
        table = database.getCollection(tableName);
        table.createIndex(new BasicDBObject(indexOn, 1));
    }

    public BasicDBObject save(BasicDBObject post) {
        table.save(post);
        return post;
    }

    public void update(BasicDBObject object) {
        ObjectId id = object.getObjectId("_id");
        object.removeField("_id");
        BasicDBObject searchCriteria = new BasicDBObject();
        searchCriteria.put("_id", id);
        table.update(searchCriteria, object);
    }

    public List<BasicDBObject> findAll() {
        DBCursor cursor = table.find();
        List<BasicDBObject> dbObjects = new ArrayList<BasicDBObject>();
        while (cursor.hasNext()) {
            dbObjects.add((BasicDBObject) cursor.next());
        }
        return dbObjects;
    }

    protected DBCollection getTable() {
        return table;
    }

    public Long count() {
        return table.count();
    }
}
