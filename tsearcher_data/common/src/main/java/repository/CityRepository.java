package repository;

import com.mongodb.DB;

/**
 * Created by Victor on 03/03/2015.
 */
public class CityRepository extends AbstractMongoRepository {
    public CityRepository(DB database) {
        super(database, "city");
    }
}
