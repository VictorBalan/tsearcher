package repository;

import com.mongodb.*;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victor on 02/03/2015.
 */
public class UserPostSavedRepository {
    DBCollection table;

    public UserPostSavedRepository(DB database) {
        table = database.getCollection("user_post_saved");
    }

    public void save(String username) {
        BasicDBObject object = new BasicDBObject();
        object.put("username", username);
        object.put("loaded", true);
        table.save(object);
    }

    public void update(String username) {
        BasicDBObject query = new BasicDBObject();
        query.put("username", username);
        DBObject object = table.findOne(query);
        BasicDBObject dbObject = (BasicDBObject) object;

        BasicDBObject searchQuery = new BasicDBObject().append("_id", dbObject.getObjectId("_id"));
        dbObject.put("loaded", true);
        table.update(searchQuery, dbObject);
    }

    public void resetAllToFalse(){
        DBCursor cursor = table.find();
        List<BasicDBObject> dbObjects = new ArrayList<BasicDBObject>();
        while (cursor.hasNext()) {
            dbObjects.add((BasicDBObject) cursor.next());
        }

        for(BasicDBObject dbObject: dbObjects) {
            BasicDBObject searchQuery = new BasicDBObject().append("_id", dbObject.getObjectId("_id"));
            dbObject.put("loaded", false);
            table.update(searchQuery, dbObject);
        }
    }

    public boolean isLoadedByUsername(String username) {
        BasicDBObject query = new BasicDBObject("username", username);
        DBObject object = table.findOne(query);
        if (object != null) {
            BasicDBObject basicDBObject = (BasicDBObject) object;
            return basicDBObject.getBoolean("loaded");
        }
        return false;
    }

    public List<BasicDBObject> findAllNotLoaded() {
        BasicDBObject query = new BasicDBObject("loaded", false);
        DBCursor cursor = table.find(query);
        List<BasicDBObject> dbObjects = new ArrayList<BasicDBObject>();
        while (cursor.hasNext()) {
            dbObjects.add((BasicDBObject) cursor.next());
        }
        return dbObjects;
    }

    public List<BasicDBObject> reinitDB() {
        DBCursor cursor = table.find();
        List<BasicDBObject> dbObjects = new ArrayList<BasicDBObject>();
        while (cursor.hasNext()) {
            dbObjects.add((BasicDBObject) cursor.next());
        }
        List<ObjectId> objectIdsToBeDeleted = new ArrayList<ObjectId>();
        for(int i=0;i<dbObjects.size();i++) {
            String username = dbObjects.get(i).getString("username");
            for (int j=0;j<dbObjects.size();j++) {
                if(j!=i){
                    if(username.compareTo(dbObjects.get(j).getString("username"))==0){
                        objectIdsToBeDeleted.add(dbObjects.get(j).getObjectId("_id"));
                    }
                }
            }
        }
        return dbObjects;
    }

}
