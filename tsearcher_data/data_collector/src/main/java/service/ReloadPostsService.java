package service;

import collector.SingleUserDataCollector;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import repository.UserPostSavedRepository;

import java.util.List;

/**
 * Created by Victor on 5/26/2015.
 */
public class ReloadPostsService {
    private SingleUserDataCollector singleUserDataCollector;
    private UserPostSavedRepository userPostSavedRepository;

    public ReloadPostsService(DB database, PostService postService){
        this.singleUserDataCollector = new SingleUserDataCollector(database, postService, true,1);
        this.userPostSavedRepository = new UserPostSavedRepository(database);
    }

    public void reloadData(){
        List<BasicDBObject> userPostSaved = userPostSavedRepository.findAllNotLoaded();

        for(BasicDBObject object: userPostSaved){
            singleUserDataCollector.saveTweetsForUser(object.getString("username"));
        }
    }
}
