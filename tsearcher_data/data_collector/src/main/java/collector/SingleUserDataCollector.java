package collector;

import com.mongodb.DB;
import repository.UserPostSavedRepository;
import service.PostService;
import twitter4j.*;
import utils.TwitterUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Victor on 02/03/2015.
 */
public class SingleUserDataCollector {
    private Twitter twitter;
    private PostService postService;
    private UserPostSavedRepository userPostSavedRepository;
    private ExecutorService executorService;
    private Boolean update = false;

    public SingleUserDataCollector(DB database, PostService postService) {
        this.postService = postService;
        this.userPostSavedRepository = new UserPostSavedRepository(database);

        TwitterFactory twitterFactory = new TwitterFactory(TwitterUtils.getConfigurationBuilder().build());
        twitter = twitterFactory.getInstance();
        executorService = Executors.newFixedThreadPool(100);
    }

    public SingleUserDataCollector(DB database, PostService postService, Boolean update, Integer threads) {
        this.postService = postService;
        this.userPostSavedRepository = new UserPostSavedRepository(database);

        TwitterFactory twitterFactory = new TwitterFactory(TwitterUtils.getConfigurationBuilder().build());
        twitter = twitterFactory.getInstance();
        this.update = update;
        executorService = Executors.newFixedThreadPool(threads);
    }

    public void saveTweetsForUser(String username) {
        executorService.execute(new CollectData(username));
    }

    private class CollectData implements Runnable {
        private String username;

        public CollectData(String username) {
            this.username = username;
        }

        public void run() {
            if (!userPostSavedRepository.isLoadedByUsername(username)) {
                System.out.println("Saving posts for " + username);
                int pageNo = 1;
                try {
                    List<Status> statusList = new ArrayList<Status>();
                    List<Status> statuses = null;
                    do {
                        //200 max results
                        Paging paging = new Paging(pageNo, 200);
                        statuses = twitter.getUserTimeline(username, paging);
                        statusList.addAll(statuses);
                        pageNo++;
                    } while (statuses.size() > 0);
                    if(update){
                        userPostSavedRepository.update(username);
                    }else {
                        userPostSavedRepository.save(username);
                    }
                    for (Status status : statusList) {
                        postService.saveWithGeoLocation(status);
                    }
                } catch (TwitterException e) {
                    int secondsUntilReset = e.getRateLimitStatus().getSecondsUntilReset() + 1;
                    try {
                        System.err.println("Twitter does not allow us to download anymore." +
                                " We`ll be back at this user in " + secondsUntilReset / 60 + " minutes. Username: " + username +
                                ". Requests made for this user: " + pageNo);
                        Thread.sleep(secondsUntilReset * 1000);
                        executorService.execute(new CollectData(username));
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            } else {
                System.out.println("Already loaded for " + username);
            }
        }
    }
}
