package collector;

import com.mongodb.DB;
import service.PostService;
import twitter4j.Status;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import utils.CustomStatusListener;
import utils.TwitterUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: Victor
 * Date: 08/01/15
 * Time: 19:41
 * To change this template use File | Settings | File Templates.
 */
public class StreamDataCollector {
    private PostService postService;
    private TwitterStream twitterStream;
    private SingleUserDataCollector singleUserDataCollector;
    private ExecutorService executorService = Executors.newFixedThreadPool(2000);

    public StreamDataCollector(DB database, PostService postService) {
        this.postService = postService;
        this.singleUserDataCollector = new SingleUserDataCollector(database, postService);

        TwitterStreamFactory twitterStreamFactory = new TwitterStreamFactory(TwitterUtils.getConfigurationBuilder().build());
        twitterStream = twitterStreamFactory.getInstance();
    }

    public void startCollector() {
        StatusListener listener = new TwitterStreamListener();
        twitterStream.addListener(listener);
        twitterStream.sample();
    }

    //******************************************************************************************************************
    private class TwitterStreamListener extends CustomStatusListener {
        @Override
        public void onStatus(Status status) {
            executorService.execute(new OnStatusThread(status));
        }
    }

    private class OnStatusThread implements Runnable {
        private Status status;

        public OnStatusThread(Status status) {
            this.status = status;
        }

        public void run() {
            boolean savedPost = postService.saveWithGeoLocation(status);
            if (savedPost) {
                singleUserDataCollector.saveTweetsForUser(status.getUser().getScreenName());
            }
        }
    }
}
