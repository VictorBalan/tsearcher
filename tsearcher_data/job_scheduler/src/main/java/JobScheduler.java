import collector.StreamDataCollector;
import com.cybozu.labs.langdetect.LangDetectException;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.document.DocumentPage;
import com.marklogic.client.io.StringHandle;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import model.KeyPhrase;
import org.json.simple.JSONObject;
import repository.UserPostSavedRepository;
import service.*;
import utils.*;

import java.security.Key;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Victor
 * Date: 08/01/15
 * Time: 19:43
 * To change this template use File | Settings | File Templates.
 */
public class JobScheduler {
    private static PostService postService;
    private static LinkService linkService;

    public static void main(String args[]) {
        try {
            LangDetect.init("./profiles");
        } catch (LangDetectException e) {
            e.printStackTrace();
        }
        initServices();
        DB database = getMongoDB();
//        startStreamCollector(database);
//        String link = "http://t.co/ii0dPPkcX8.,�\n" +
//                "\n" +
//                "@CahillJimb0";
//        DataMiningService dataMiningService = new DataMiningService(linkService);
//        System.out.print(URLUtils.getUrls(link));

        MigrationService migrationService = new MigrationService(postService, linkService);
        try {
            migrationService.startGeolocationMigration();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void initServices(){
        DatabaseClient databaseClient = MarkLogicUtils.newDatabaseClient(DatabaseClientFactory.Authentication.DIGEST);

        linkService = new LinkService(databaseClient);
        postService = new PostService(databaseClient, linkService);
    }

    private static DB getMongoDB() {
        try {
            MongoUtils.startMongo();
            MongoClient mongoClient = new MongoClient("localhost", 27017);
            return mongoClient.getDB("twitter_data_collector");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void startStreamCollector(DB database) {
        try {
            StreamDataCollector streamDataCollector = new StreamDataCollector(database, postService);
            streamDataCollector.startCollector();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
