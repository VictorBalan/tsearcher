Java este un limbaj de programare orientat-obiect, puternic tipizat, conceput de c?tre James Gosling la Sun Microsystems (acum filial? Oracle) la �nceputul anilor ?90, fiind lansat �n 1995. Cele mai multe aplica?ii distribuite sunt scrise �n Java, iar noile evolu?ii tehnologice permit utilizarea sa ?i pe dispozitive mobile gen telefon, agenda electronic?, palmtop etc. �n felul acesta se creeaz? o platform? unic?, la nivelul programatorului, deasupra unui mediu eterogen extrem de diversificat. Acesta este utilizat �n prezent cu succes ?i pentru programarea aplica?iilor destinate intranet-urilor[10].

Limbajul �mprumut? o mare parte din sintax? de la C ?i C++, dar are un model al obiectelor mai simplu ?i prezint? mai pu?ine facilit??i de nivel jos. Un program Java compilat, corect scris, poate fi rulat f?r? modific?ri pe orice platform? care e instalat? o ma?in? virtual? Java (englez? Java Virtual Machine, prescurtat JVM). Acest nivel de portabilitate (inexistent pentru limbaje mai vechi cum ar fi C) este posibil deoarece sursele Java sunt compilate �ntr-un format standard numit cod de octe?i (englez? byte-code) care este intermediar �ntre codul ma?in? (dependent de tipul calculatorului) ?i codul surs?.

Ma?ina virtual? Java este mediul �n care se execut? programele Java. �n prezent, exist? mai mul?i furnizori de JVM, printre care Oracle, IBM, Bea, FSF. �n 2006, Sun a anun?at c? face disponibil? varianta sa de JVM ca open-source.

Exist? 4 platforme Java furnizate de Oracle:

Java Card - pentru smartcard-uri (carduri cu cip)
Java Platform, Micro Edition (Java ME) � pentru hardware cu resurse limitate, gen PDA sau telefoane mobile,
Java Platform, Standard Edition (Java SE) � pentru sisteme gen workstation, este ceea ce se g?se?te pe PC-uri,
Java Platform, Enterprise Edition (Java EE) � pentru sisteme de calcul mari, eventual distribuite.